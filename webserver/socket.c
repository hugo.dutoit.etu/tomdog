#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <stdio.h>
#include <arpa/inet.h>
#include "socket.h"

int creer_serveur( int port ){
	int serveur =  socket(AF_INET, SOCK_STREAM, 0);
	if(serveur == -1){
		perror("creation serveur");
	}
	struct sockaddr_in saddr;
	saddr.sin_family = AF_INET;/* Socket ipv4 */
	saddr.sin_port = htons(port);/* Port d'écoute */
	saddr.sin_addr.s_addr = INADDR_ANY;/* écoute sur toutes les interfaces */
	int optval = 1;
	if (setsockopt(serveur, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(int)) == -1)
		perror("Can not set SO_REUSEADDR option");
	if(bind(serveur , (struct sockaddr *)&saddr, sizeof(saddr)) == -1){
		perror("bind serveur");
	}
	if(listen(serveur,10 == -1)){
		perror("listen serveur");
	}
	return serveur;
}
