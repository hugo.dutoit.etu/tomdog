#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include "socket.h"
#include "http_parse.h"

#define BUFFER_SIZE 1000

char *MessageBienvenu = "Bonjour cher client, bienvenue sur le serveur tomdog \nSur ce serveur, vos aboiements seront répliqués donc arrêtez de barker. \n Ici, vous serez facturés de 5 balles par message envoyé au serveur. \n Merci d'avance de régaler tout le chenil. \nToute l'équipe TomDog vous souhaite la bienvenue dans le chenil \n Hugo \nMartin \n\" TomDog votre compagnon de tous les jours \" \r\n ";

void traitement_signal()
{
	while ((waitpid(-1, NULL, WNOHANG)) > 0)
		;
}
void initialiser_signaux(void)
{
	if (signal(SIGPIPE, SIG_IGN) == SIG_ERR)
		perror("signal");
	struct sigaction sa;
	sa.sa_handler = traitement_signal;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;
	if (sigaction(SIGCHLD, &sa, NULL) == -1)
		perror("sigaction(SIGCHLD)");
}


char *getExtention(char *fichier){
	char *ext= malloc(strlen(fichier));
	strcpy(ext,fichier);
	for(;*ext!='.'; ext++);
	ext++;
	printf("%s",ext);

	return ext;
}
int isNumeric(char *arg)
{
	int i = 0;
	while (arg[i] != '\0')
	{
		if (arg[i] < 48 || arg[i] > 57)
		{
			return 0;
		}
		i++;
	}
	return 1;
}

char *fgets_or_exit(char *buffer, int size, FILE *stream)
{
	char *res = fgets(buffer, size, stream);
	if (res == NULL){
		perror("SERVER : ERROR while reading client Socket line");
		exit(1);
	}
	return res;
}

void skip_header(FILE *IOClient)
{
	// Ici on vide l'entete de connection jusqu'a recevoir la ligne \r\n
	char buffer[BUFFER_SIZE];
	do
	{
		fgets_or_exit(buffer, BUFFER_SIZE, IOClient);
	} while (strcmp(buffer, "\r\n") != 0 && strcmp(buffer, "\n") != 0);
}

void send_status(FILE *client, int code, const char *reason_phrase)
{
	printf( "SERVER SEND :HTTP/1.1 %d %s\r\n", code, reason_phrase);
	fprintf(client, "HTTP/1.1 %d %s\r\n", code, reason_phrase);
}

void send_response(FILE *client, int code, const char *reason_phrase, const char *message_body)
{
	send_status(client, code, reason_phrase);
	printf("SERVER SEND :Content-Length:%ld\r\n", strlen(message_body));
	fprintf(client, "Content-Length:%ld\r\nConnection: closed\r\n", strlen(message_body));
	fprintf(client,"%s\r\n\r\n",message_body);
}

char *getContentType(char *fichier){
	char *ext=getExtention(fichier);
	if(strcmp(ext,"html")==0){
		return "text/html";
	}
	if(strcmp(ext,"jpeg")==0){
		return "image/jpeg";
	}
	return "text";
}

char *rewrite_target(char *target)
{
	int id = 0;
	for (; target[id] != '?' && id < (int)strlen(target); id++){
		printf(">%c\n",target[id]);
	};
	target[id] = '\0';
	if(strstr(target,"../")!=NULL){
		return NULL;
	}
	if(strcmp(target,"/")==0){
		target="/index.html";
	}
	return target;
}

int copy(FILE *in, FILE *out){
	int cpt=0;
	short int read_val;
	char buffer[BUFFER_SIZE];
	do{
		read_val = fread(buffer,sizeof(short int), BUFFER_SIZE, in);
		fwrite(buffer,sizeof(short int),read_val,out);
		printf("\n>%d<\n",read_val);
		cpt+= read_val;
	}
	while (read_val == BUFFER_SIZE);
	fclose(in);
	return cpt;
}

// fonction permettant de verifier que le chemin passé en paramettre et bien un dossier et qu il est accessible
int goodWebRepository(const char *webRepo){
	struct stat *inode=malloc(sizeof(struct stat));
	if(stat(webRepo,inode) == -1){
		perror(webRepo);
		return -1;
	}else if(!S_ISDIR(inode->st_mode)){
		perror("not a directory");
		return -1;
	}
	return 1;
}

FILE *check_and_open(const char*target,const char*document_root){
	char *path = malloc(strlen(target)+strlen(document_root));
	strcpy(path,document_root);
	strcat(path,target);
	FILE *res = fopen(path,"r");
	if(res == NULL){
		perror("No Such File");
	}
	return res;
}

int get_file_size(int fd){
	struct stat *data= malloc(sizeof (struct stat));	
	fstat(fd,data);
	return data->st_size;
}

////////////////////////////////////////////////////////////////////
//////////// MAIN FUNCTION /////////////////////////////////////////
////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
	printf("Server Starting...\r\n");

	// Permet de detexter quand les processus fils se zombifie et les kills
	initialiser_signaux();
	int serveur;
	char *webRoot;

	//On test que les paramettres passé est un chemin 
	if (argc > 2) {

		//On test si le premier paramettre est un nombre
		if(!isNumeric(argv[1])){
			perror("le port doit uniquement être composé de chiffres");
			return 0;
		}

		// on test si le deuxieme paramettre est un dossier et qu'il est accessible
		if(goodWebRepository(argv[2])<0){
			perror("Error while reading param 2");
			return 0;
		}
	}
	else
	{
		perror("Wrong number on argument");
		return 0;
	}

	webRoot = argv[2];
	// Initialisation du Serveur
	printf("Serveur Creating...\r\n");
	serveur = creer_serveur(atoi(argv[1]));

	printf("Server Started !!!\r\n");

	//gestion de L'Arrivée des clients
	int socketclient;
	int fils;

	// Cette boucle while sert a accueillir plusieur client a la suite
	while (1)
	{
		socketclient = accept(serveur, NULL, NULL);
		if (socketclient == -1)
			perror("socketclient");
		FILE *IOClient = fdopen(socketclient, "a+");

		//Ce Fork sert a accueillir plusieur client en meme temps
		fils = fork();
		// Tous ce qui est dans ce if s'excute uniquement pour le client lors de sa connection
		if (fils == 0)
		{

			//Variable local au client
			char buffer[BUFFER_SIZE];
			http_request *entete_requete = malloc(sizeof(http_request));
			FILE *data;

			//fonction de récupperation des messages du client
			fgets_or_exit(buffer, BUFFER_SIZE, IOClient);

			// Ligne de debuggage permettant de comprendre la ligne que lis le serveur
			printf("Test Serveur Reception ligne : %s\r\n", buffer);

			//on recupere l'entete de la requete dans la premier ligne partager par le client
			int parse_req_status = parse_http_request(buffer, entete_requete);

			//On passe la suite de l entête
			skip_header(IOClient);
			
			char *targetWithoutQuery;
			//Ici on analyse la methode de la requete
			//en ce moment on ne traite que la requete GET donc tout autre requete sera rejeter avec le code erreur 400 Bad Request
			if (parse_req_status == -1)
			{
				if (entete_requete->method == HTTP_UNSUPPORTED)
				{
					send_response(IOClient, 405, "Method Not Allowed", "Method Not Allowed\r\n");
					exit(1);
				}
				else
				{
					send_response(IOClient, 400, "BAD REQUEST", "BAD REQUEST\r\n");
					exit(1);
				}
			}
			//Ici on test si la target existe
			else if((targetWithoutQuery=rewrite_target(entete_requete->target))!=NULL){
				if ( (data = check_and_open(targetWithoutQuery,webRoot)) != NULL)
				{
					//Si l'entete de requête est bonne on envois le message de bienvenu
					// send_response(IOClient, 200, "OK", );
					send_status(IOClient,200,"0K");
					fprintf(IOClient,"Content-Length:%d\r\nContent-Type: %s\r\nConnection: close\r\n\r\n",get_file_size(fileno(data)),getContentType(targetWithoutQuery));
					copy(data,IOClient);
				}
				else
				{
					//si la target de la requete est mauvaise on renvoie le code 404
					send_response(IOClient, 404, "NOT FOUND", "NOT FOUND\r\n\r\n");
					exit(1);
				}
			}else{
				printf("SERVER SEND:403 FORBIDDEN");
				send_response(IOClient, 403, "FORBIDDEN", "403 Forbidden\r\n\r\n");
				exit(1);
			}


			
			// fprintf(IOClient, "%s", MessageBienvenu);
			// printf("SERVER: Start Reading Client content\r\n");
			// // Boucle de lecture des message
			// while (strlen(fgets_or_exit(buffer, BUFFER_SIZE, IOClient)) != 0)
			// {
			// 	fprintf(IOClient, "tomdog :%s", buffer);
			// 	printf("SERVER: read->%s from Client\r\n", buffer);
			// 	fflush(IOClient);
			// }
			exit(1);
		}
	}
}
